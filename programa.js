// crear un objeto (variable compuestas) para representar el visor
//L representa a la biblioteca leaflet
let miMapa = L.map("mapid");

//Determinar la vista inicial
miMapa.setView([4.628039364258417, -74.0658076107502], 16);
let miProveedor = L.tileLayer(
  "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
);
miProveedor.addTo(miMapa);


let miGeoJSON= L.geoJSON(sitio);

miGeoJSON.addTo(miMapa);

let miTitulo=document.getElementById("titulo");
miTitulo.textContent=sitio.features[0].properties.popupContent;

let miDescripcion=document.getElementById("descripcion");
miDescripcion.textContent=sitio.features[0].properties.description;

