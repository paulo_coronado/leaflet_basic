let sitio=
{
  "type": "FeatureCollection",
  "features": [
       {
      "type": "Feature",
      "properties": {
        "popupContent":"Este es mi cultivo de tomates",
        "description":"Esta es la descripción de mi cultivo"
      },
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              -74.06489968299866,
              4.626516825042151
            ],
            [
              -74.0659511089325,
              4.622335506474684
            ],
            [
              -74.0623140335083,
              4.621672481826402
            ],
            [
              -74.06162738800049,
              4.624805802525023
            ],
            [
              -74.06280755996704,
              4.626506131164262
            ],
            [
              -74.06489968299866,
              4.626516825042151
            ]
          ]
        ]
      }
    }
  ]
};
